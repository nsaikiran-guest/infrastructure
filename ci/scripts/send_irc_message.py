#!/usr/bin/python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Send a message to irc channel.
"""

import argparse
import datetime
import irc.bot
import irc.strings
import os
import ssl


class SendBot(irc.bot.SingleServerIRCBot):
    """Bot to send a message and quit."""

    def __init__(self, server, port, nickname, channel, message):
        """Initialize the bot object."""
        ssl_factory = irc.connection.Factory(wrapper=ssl.wrap_socket)

        print('Connecting to server: ircs://{}:{}'.format(server, port))
        super().__init__(
            [(server, port)], nickname, nickname, connect_factory=ssl_factory)
        self.channel = channel
        self.message = message

    def on_nicknameinuse(self, connection, event):
        """If nickname is in use, append underscore to it."""
        old_nick = connection.get_nickname()
        new_nick = old_nick + '_'
        print('Nick {} is use. Changeing to {}'.format(old_nick, new_nick))
        connection.nick(new_nick)

    def on_welcome(self, connection, event):
        """On successful connection, join a channel."""
        print('Connected. Joining channel: {}'.format(self.channel))
        connection.join(self.channel)

    def on_join(self, connection, event):
        """On successful join, send the message."""
        print('Joined channel.')
        for line in self.message.splitlines():
            print('Sending message:', line)
            connection.privmsg(self.channel, line)

        print('Quitting.')
        connection.quit('Bye.')

    def on_disconnect(self, connection, event):
        """On disconnection, exit."""
        raise SystemExit()


def parse_arguments():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--server', help='Server to connect to', required=True)
    parser.add_argument('--port', default=6667, type=int, required=True)
    parser.add_argument(
        '--channel', help='Channel to post message in', required=True)
    parser.add_argument(
        '--nick', help='IRC nick to post message as', required=True)
    parser.add_argument('--message', help='Message to send', required=True)
    args = parser.parse_args()
    return {
        'server': args.server,
        'port': args.port,
        'nick': args.nick,
        'channel': args.channel,
        'message': args.message
    }


def get_args():
    """Get the input arguments to the script from Go CD environment."""
    pipeline_name = os.environ['GO_PIPELINE_NAME']
    success = 'success' if os.path.isfile('passed') else 'FAILED'
    timestamp = datetime.datetime.now().isoformat()
    message = '{} - {} ({})'.format(success, pipeline_name, timestamp)
    return {
        'server': os.environ['IRC_SERVER'],
        'port': int(os.environ['IRC_PORT']),
        'nick': os.environ['IRC_NICK'],
        'channel': os.environ['IRC_CHANNEL'],
        'message': message
    }


def main():
    """Connect to IRC server and post a message in a channel."""
    try:
        args = get_args()
    except (KeyError, ValueError):
        args = parse_arguments()

    bot = SendBot(args['server'], args['port'], args['nick'], args['channel'],
                  args['message'])
    bot.start()


if __name__ == '__main__':
    main()
